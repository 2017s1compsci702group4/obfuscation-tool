#!/bin/bash

if [ $# -eq 1 ]; then
    keytool -genkey -v -keystore $1.jks -keyalg RSA -keysize 2048 -validity 10000 -alias $1
fi

