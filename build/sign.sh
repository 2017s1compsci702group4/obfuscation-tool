#!/bin/bash

if [ $# -eq 2 ]; then
    apksigner sign --ks $1 --out new-$2 $2
fi
