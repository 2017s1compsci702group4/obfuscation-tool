Android Obfuscation Tool for COMPSCI 702 | Group Project

# General

The obfuscation tool encrypts strings in the CryptoMe application. Strings are decrypted dynamically at runtime.

Key methods are implemented using reflection calls. This allows for code to be represented using strings - allowing method calls to be encrypted. This is combined with manual obfuscation of business critical constants, further complicating static analysis.

# How to use
1. Build the application.
2. Run `get_sig.sh {your-apk}.apk > signature.txt`
3. Update the SIG variable in the obfuscator.py file with the new signature.
4. Run `python3 obfuscator.py /path/to/input/.java/files /path/to/output/folder`
5. Build and install.