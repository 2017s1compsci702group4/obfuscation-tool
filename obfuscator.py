import os
import re
import base64
import random, string
import sys

"""
Obfuscation script for encrypting strings in Java source code.

This script is designed to be combined with the ResourceProvider.class file. 

Strings are encrypted using a complex algorithm, frustrating static analysis of the compiled .apk file.

The ResourceProvider class decrypts the strings at runtime.

Correct usage:
python3 convertStrings.py input_directory output_directory
"""

SIG = "308202c3308201aba00302010202047e9314f3300d06092a864886f70d01010b050030123110300e0603550403130747726f75702034301e170d3137303432343034353331335a170d3432303431383034353331335a30123110300e0603550403130747726f7570203430820122300d06092a864886f70d01010105000382010f003082010a0282010100a1bc27636c41da5fb2bfb2effc01b3955df090e750312b867a6042136ed09746f89e63057e76db3161ae32706d57e7b0f7200cbe210e14293fa75883dd26addbbfeda17f2be70ff957081946a25c62f6c2da6d376130f56e5de52c4a448680fac61bbddfe6ca043ea4458893dfb113ad3a77bcff3e2e76d0cf78060a821cd5afc73b4e14857e6e722e7134b58db456f9f29304e5c127bb3bdad73c3c9437260113d3ac739c6290a2186de6774067efcce6725e38261876dd2422f3041063d8cc5aca1bff2f0ba3f65a331235d683fbedc8a90af1080c2b108f2d123adcffff43d58d23482ecd3644bdaec50d906a741f0082c5f160020a01010d4c55f81102f30203010001a321301f301d0603551d0e041604140bb4c519177139676bfa8bc36363b58e33586a0a300d06092a864886f70d01010b050003820101006da3dd5c47ce4afc9dddcdc88c7898aa77fd02a8e0266aee32106cb9ca9b20e7e2d533b9786db5b040ef291786ed95b7129945746a5d992342dfd4d558bf2ba80ad69f3dde6e78c6c7aa976877b6749ec10c92e0fbbb7d1cd9e17afe1b1ab9fa2bc8a113dcaa45e97bba5e74d00f396b3dbdb377dbbde2b80b6927ff7cf097e0a7a63bafcbe4fbe737fd38ae6e4f82f0b24419a04d214b0b1a700ad7aeef410f9f469a4f98d62da985536046a5ff8483ac07a16c06cb902650db68f8a18599238feaa9657627b721b77bfa4c7fe3434fdf5e227aa245a01947a4114f2562813ab7261e3195cdb55bcb9a3c4d39f41d11ef19bf58e20c12a246de66eda24151f1"

key = None

"""
Shift the key by 30 + i ASCII characters altering direction with every increment of i 
Where i < keylength && i++ per loop
"""
def get_key():
    global key, SIG
    if key == None:
        key = ascii_shift(SIG, 30, True)
    return key

"""
Encrypt strings in Java source files.
"""
def convert_strings(files, output_file_path):
    for file in files:
        nm = file.split('/')[-1]
        output_path = output_file_path + nm # destination of obfuscated file
        make_file_if_not_exists(output_path)
        with open(file) as input_file, open(output_path, 'w') as output_file:
            for line in input_file:
                new_line = line;
                matches = re.findall('\"([a-zA-z0-9\s\,\.\(\)\!\-\=\+\/)]*?)\"', line) # find all instances of strings in the file
                if file.find("PackageName.java") == -1: # for any file other than the required resource provider class
                    for match in matches: # for all of the strings in that file
                        if match != "":
                            encoded_string = encode_string(match)
                            replacement = "ResourceProvider.getString(\"{}\")".format(encoded_string)
                            new_line = new_line.replace("\"" + match + "\"", replacement)
                output_file.write(new_line)

"""
Encode a given string.
"""
def encode_string(original):
    key = get_key()
    encoded = xor_with_key(original, key)
    encoded = base64.b64encode(encoded)
    encoded = encoded.decode(encoding='UTF-8')
    encoded = ascii_shift(encoded, 30)
    encoded = encoded.replace(" \ ".strip(), " \ \ ".replace(" ", ""))
    encoded = encoded.replace('"', '\\"')
    return encoded

"""
Perform logical XOR on given strings.
"""
def xor_with_key(string, key):
    str_bytes = string.encode(encoding='UTF-8')
    key_bytes = key.encode(encoding='UTF-8')
    result_bytes = bytearray()
    for i,c in enumerate(str_bytes):
        result_bytes.append(c ^ key_bytes[i % len(key)])
    return result_bytes

"""
Shift characters in given ASCII string forward or backward by a specified value + i
Where i = current cursor in string.
"""
def ascii_shift(string, base_shift, plus = True):
    new_string = ""
    for i,c in enumerate(string):
        ascii = ord(c)
        if plus:
            ascii += (base_shift + i)
            plus = False
        else:
            ascii -= (base_shift + i)
            plus = True
        while ascii < 32:
            ascii = 127 - (32 - ascii);
        while ascii > 126:
            ascii = 31 + (ascii - 126);
        new_c = chr(ascii)
        new_string += new_c
    return new_string

"""
Get a random sequence of ASCII characters of specified length.
"""
def random_word(length):
   return ''.join(random.choice(string.ascii_uppercase) for i in range(length))

"""
Get list of Java files in specified folder.
"""
def get_java_files_from_directory(root_folder_path):
    files = []
    for dir_path, _, file_names in os.walk(root_folder_path):
        for file_name in [f for f in file_names if f.endswith(".java")]:
            files += [os.path.join(dir_path, file_name)]
    return files

"""
Creates a file at the specified location if it does not already exist.
"""
def make_file_if_not_exists(file_path):
    if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))

def main():
    args = sys.argv[1:]
    if len(args) == 2:
        input_file_path = args[0]
        output_file_path = args[1]
        files = get_java_files_from_directory(input_file_path)
        convert_strings(files, output_file_path)
    else:
        print('Usage: obfuscator.py [input_directory] [output_directory]')

if __name__ == "__main__":
    main()
