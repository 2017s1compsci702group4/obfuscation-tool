#!/bin/bash

if [ $# -eq 1 ]; then
    apk=$1
    zip=${apk/%.apk/.zip}
    dex=app-dex/${apk/%.apk/.dex}
    jar=app-jar/${apk/%.apk/.jar}
    mv $apk $zip && unzip -o -q $zip classes.dex
    mv $zip $apk && mv classes.dex $dex
    dex2jar-2.0/d2j-dex2jar.sh --force $dex -o $jar
fi
